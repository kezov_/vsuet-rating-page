export default {
  API_URL: process.env.NODE_ENV === 'production' ? 'https://rating.kenan.agency' : 'https://localhost:8646' //
}
